
// single-thread task resource thing
// ... for OpenGL windows and Node.JS like resource control

// `#define SOLOTASK_CPP` in ONE C++ file to create the body

#ifndef _SOLOTASK_H_
#define _SOLOTASK_H_


#ifdef __cplusplus

// C++ has a convience function ...
#include <functional>

// i'm using unique pointers
#include <memory>

#ifdef SOLOTASK_CPP
// the body uses stl list and assert
#include <list>
#include <assert.h>
#include <thread>
#include <mutex>
#endif


// the public API is C
extern "C" {
#endif

	// the three types
	typedef void* st_queue_p; // a pretty type for the queue
	typedef void* st_host_p; // a pretty type for the queue
	typedef void* st_userdata_p; // a pretty type for the task itself
	typedef void (*st_callback_f)(st_queue_p, st_host_p, st_userdata_p); // the callback. includes the host for negging reasons

	// with those defined we/i can create the C-API for clients
	// these are expressed as function pointers so they can be privately setup by C++
	//		this is a subversion of my usual feelings on global variables

	// add a task to the queue.
	//
	// - need a queue pointer
	// - can have some userdata
	// - need a function to execute which also needs to free the userdata as part of itself
	//		... if that data needs to be freed in a different thread, then, that needs to be done in the task
	void st_enqueue(st_queue_p, st_userdata_p, st_callback_f);

	// now ... the three signals that the queue can return (without blowing up)
	typedef enum {
		st_not_empty_e = 0, // we have processed a task, but, there are more to do
		st_was_empty_e, // there are no more events
		st_now_empty_e, // there were events, but, there are no more
	} st_result_e;

	// process some task from a queue
	st_result_e st_process(st_queue_p);


#ifdef __cplusplus // for body
};



struct st final
{
	st_queue_p grab(void) noexcept { return reinterpret_cast<void*>(this); }

	st(const st&) = delete; // non construction-copyable
	st& operator=(const st&) = delete; // non copyable

	// creation stuff
	~st(void)noexcept;

	// create one ... maybe?	
	static std::unique_ptr<st> create(void* userdata = nullptr, void(*userfree)(void*) = nullptr)noexcept;

#ifdef SOLOTASK_CPP
	st(void*, void(*)(void*))noexcept;

	struct task
	{
		st_callback_f _callback;
		st_userdata_p _userdata;
	};

	std::list<task> _tasks;
	void* _userdata;
	void(*_userfree)(void*);
	std::mutex _lock;

#endif // SOLOTASK_CPP

	template<typename T>
	class res;

private:
	// ugly nasty little thing
	void* userdata(void)noexcept;
public:

	/// <summary>
	/// type class - you can specialise these to get specific behaviour
	/// </summary>
	/// <typeparam name="T"></typeparam>
	template<typename T>
	struct mixin final
	{
		static void prepare(res<T>&, T*)noexcept {}
		static void conclude(res<T>&, T*)noexcept {}
	};

	template<typename T>
	class res final
	{
		std::unique_ptr<st> _st;
	public:
		res(const res<T>&) = delete; // non construction-copyable
		res<T>& operator=(const res<T>&) = delete; // non copyable

		st* operator->(void)noexcept { return _st.get(); }

		res(T* userdata, void(*userfree)(T*))noexcept
		{
			_st = st::create((void*)userdata, (void(*)(void*))userfree);
		}

		res(T& userdata)noexcept
		{
			_st = st::create(&userdata, nullptr);
		}

		~res(void)noexcept {}

		void enqueue(std::function<void(T*)> code)noexcept
		{
			st_enqueue(
				_st->grab(),
				reinterpret_cast<st_userdata_p>(new std::function<void(T*)>(code)),
				[](st_queue_p self, void* host, st_userdata_p data)
				{
					std::function<void(T*)>* code = reinterpret_cast<std::function<void(T*)>*>(data);
					(*code)(reinterpret_cast<T*>(host));
					delete data;
				}
			);
		}

		void enqueue(std::function<void(T&)> code)noexcept
		{
			st_enqueue(
				_st->grab(),
				reinterpret_cast<st_userdata_p>(new std::function<void(T&)>(code)),
				[](st_queue_p self, void* host, st_userdata_p data)
				{
					std::function<void(T&)>* code = reinterpret_cast<std::function<void(T&)>*>(data);
					(*code)(*(reinterpret_cast<T*>(host)));
					delete data;
				}
			);
		}


		operator st_queue_p(void) noexcept { return _st.get()->grab(); }


		/// special version of process with EXTRA stuff
		/// - runs prepare/conclude to switch T on/off (ALWAYS - dues to limitations)
		/// - runs the same value up-to LIMIT times (or ALL if limit == 0)
		/// - regardless of the above; alwaysreturns the last result value
		st_result_e process(const size_t limit = 1)
		{
			mixin<T>::prepare(*this, reinterpret_cast<T*>(_st->userdata()));

			auto last = st_process(_st->grab());

			for (size_t count = 1;
				// while count is not equal to limit
				(count != limit)
				&&
				// the last result was not an empty
				(st_result_e::st_not_empty_e == last);

				count++)
			{
				last = st_process(_st->grab());
			}

			mixin<T>::conclude(*this, reinterpret_cast<T*>(_st->userdata()));

			return last;
		}
	};
};
#endif __cplusplus // for body



#ifdef SOLOTASK_CPP

// this block WILL NOT compile if you include it in a C file; only C++!

std::unique_ptr<st> st::create(void* userdata, void(*userfree)(void*))noexcept
{
	return std::make_unique<st>(userdata, userfree);
}

st::st(void* userdata, void(*userfree)(void*))noexcept :
	_userdata(userdata),
	_userfree(userfree)
{
}

void st_enqueue(st_queue_p that, st_userdata_p data, st_callback_f call)
{
	auto self = reinterpret_cast<st*>(that);

	st::task task = { call, data };

	assert(task._callback && "blank tasks are a dumb");

	std::lock_guard<std::mutex> guard(self->_lock);

	auto len = self->_tasks.size();

	self->_tasks.emplace_back(task);

	auto len2 = self->_tasks.size();
	auto len3 = self->_tasks.size();
	
}

st_result_e st_process(st_queue_p that)
{
	auto self = reinterpret_cast<st*>(that);

	// could lock "process" too i guess ... maybe ... nahh

	st::task task;
	// grab the task then release the queue
	{
		std::lock_guard<std::mutex> guard(self->_lock);
		// check if there are any tasks to do
		if (self->_tasks.empty())
			return st_result_e::st_was_empty_e;

		// remove a task from the front
		task = self->_tasks.front();
		self->_tasks.pop_front(); // do this NOW to avoid dumb stuff
	}

	assert(task._callback && "blank tasks are a dumb");

	// execute the task
	task._callback(that, self->_userdata, task._userdata);

	// return the current state
	std::lock_guard<std::mutex> guard(self->_lock);
	return self->_tasks.empty() ? st_result_e::st_now_empty_e : st_result_e::st_not_empty_e;
}



st::~st(void)noexcept
{
	assert(_tasks.empty() && "tried to close queue before it was empty");

	if (_userfree)
		_userfree(_userdata);
}

void* st::userdata(void) noexcept
{
	return _userdata;
}



#endif // SOLOTASK_CPP
#endif // _SOLOTASK_H_
