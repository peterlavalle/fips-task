
#include <catch_amalgamated.hpp>

#include "dukpp.hpp"
#include "solotask.h"


TEST_CASE("test up/down duk thing")
{
	st::res<duk_context> ctx(duk_create_heap_default(), duk_destroy_heap);

}

template<typename T>
struct odec
{
	static void push(duk_context*, T&)noexcept;
	static T read(duk_context*, duk_idx_t)noexcept;
	static bool is_a(duk_context*, duk_idx_t)noexcept;
};



TEST_CASE("big duktap test")
{

	st::res<duk_context> ctx(duk_create_heap_default(), duk_destroy_heap);

	// setup a variable
	int the_variable = 27;

	// enqueu the global set function
	ctx.enqueue(
		[&the_variable](duk_context* ctx)
		{
			duk::push<int>(ctx, 1, the_variable,
				[](duk_context* ctx, int& the_variable) -> duk_ret_t
				{
					// just copy the value
					the_variable = duk_require_int(ctx, 0);
					return 0;
				}
			);
			duk_put_global_string(ctx, "callfoo");
		}
	);

	// check a variable
	REQUIRE(27 == the_variable);

	// process the queue
	REQUIRE(st_result_e::st_now_empty_e == st_process(ctx));
	REQUIRE(st_result_e::st_was_empty_e == st_process(ctx));

	// check a variable
	REQUIRE(27 == the_variable);

	// enqueue command
	ctx.enqueue(
		[](duk_context* ctx)
		{
			duk_eval_string_noresult(ctx, "callfoo(32)");
		}
	);

	// check a variable
	REQUIRE(27 == the_variable);

	// process the queue
	REQUIRE(st_result_e::st_now_empty_e == st_process(ctx));
	REQUIRE(st_result_e::st_was_empty_e == st_process(ctx));

	// check a variable is updated
	REQUIRE(32 == the_variable);

	// enqueue two commands
	ctx.enqueue([](duk_context* ctx) { duk_eval_string_noresult(ctx, "callfoo(23)"); });
	ctx.enqueue([](duk_context* ctx) { duk_eval_string_noresult(ctx, "callfoo(64)"); });

	// process all the commands
	REQUIRE(st_result_e::st_not_empty_e == st_process(ctx));
	REQUIRE(st_result_e::st_now_empty_e == st_process(ctx));
	REQUIRE(st_result_e::st_was_empty_e == st_process(ctx));

	// final check of variable
	REQUIRE(64 == the_variable);
}
