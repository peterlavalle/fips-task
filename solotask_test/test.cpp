

#include <iostream>

#include "test.hpp"

#include "solotask.h"



void main()
{
	void test1(void);
	void test2(void);
	void test3(void);
	void test4(void);
	void test5(void);

	auto mine = st::create();

	std::cout << "let's go children!" << std::endl;
	{
		test1();

		test2();

		test3();

		test4();
		test5();
	}
	std::cout << "that's all tests DONE!" << std::endl;
}

void test1(void)
{
	auto mine = st::create();
	bool b = mine ? true : false;
	CHECK(mine);
}

void test2(void)
{
	auto mine = st::create();

	CHECK(st_result_e::st_was_empty_e == st_process(mine->grab()));
	CHECK(st_result_e::st_was_empty_e == st_process(mine->grab()));
}
