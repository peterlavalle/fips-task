

#include "test.hpp"


void test3(void)
{
	auto mine = st::create();

	int count = 0;

	CHECK(0 == count);

	st_enqueue(
		mine->grab(),
		&count,
		[](void*, void*, void* data)
		{
			CHECK(0 == (*reinterpret_cast<int*>(data)));
			(*reinterpret_cast<int*>(data)) = 7;
			CHECK(7 == (*reinterpret_cast<int*>(data)));
		}
	);
	CHECK(0 == count);

	CHECK(st_result_e::st_now_empty_e == st_process(mine->grab()));
	CHECK(7 == count);
	CHECK(st_result_e::st_was_empty_e == st_process(mine->grab()));
	CHECK(7 == count);
}

void test4(void)
{
	int count = 0;
	CHECK(0 == count);

	// create an instance
	st::res<int> wrap(count);
	CHECK(0 == count);

	wrap.enqueue([](int& count)
		{
			CHECK(0 == count);
			count = 71;
			CHECK(71 == count);
		}
	);
	CHECK(0 == count);

	CHECK(st_result_e::st_now_empty_e == st_process(wrap->grab()));
	CHECK(71 == count);
	CHECK(st_result_e::st_was_empty_e == st_process(wrap->grab()));
	CHECK(71 == count);

}

struct state_s
{
	int _count = 0;
	bool _ready = false;
};

void st::mixin<state_s>::prepare(st::res<state_s>&, state_s* state)noexcept
{
	CHECK(!state->_ready);
	state->_ready = true;
}
void st::mixin<state_s>::conclude(st::res<state_s>&, state_s* state)noexcept
{
	CHECK(state->_ready);
	state->_ready = false;
}

void test5(void)
{
	state_s state;

	CHECK(0 == state._count);
	CHECK(!state._ready);

	// create an instance
	st::res<state_s> res(state);

	res.enqueue([](state_s& state)
		{
			CHECK(0 == state._count);
			CHECK(state._ready);
			state._count = 71;
		}
	);
	CHECK(!state._ready);


	CHECK(st_result_e::st_now_empty_e == res.process());
	CHECK(71 == state._count);
	CHECK(!state._ready);
	CHECK(st_result_e::st_was_empty_e == res.process());
}


