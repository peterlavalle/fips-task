
#include <iostream>

#include "solotask.h"

#define CHECK(CON) \
	do { \
		if ((CON)) break; \
		std::cerr << std::endl << "ERROR\t! " << __FILE__ << ":" << __LINE__ << "\n\t!\t" << (#CON) << std::endl << std::endl; \
		exit(EXIT_FAILURE); \
	} while(false)

